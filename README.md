# Fedora Mentor Summit

Fedora Mentor Summit is an event for Fedora Community Members interested in Mentorship as a practice to share experiences, learn, grow, and network while having fun.

## What is this place?
This repository is our planning and task tracker for the Fedora Mentor Summit.

## About Fedora Mentor Summit (FMS) 2025
TBA

## How to contact the team

Other than this repository, you can also find us on the DEI team Matrix room:

* [Matrix](https://matrix.to/#/#dei:fedoraproject.org)

## Previous editions
* 2024: in-person event, part of the Flock to Fedora conference in Rochester, New York, United States of America - [videos](https://www.youtube.com/playlist?list=PL0x39xti0_6757itYwFBHqaZRdIiF43jO) and [schedule](https://cfp.fedoraproject.org/flock-2024/schedule/)
* 2023: in-person event, part of the Flock to Fedora conference in Cork, Ireland - [videos](https://www.youtube.com/playlist?list=PL0x39xti0_64OcXEGLCtoI4nouADqaTcT) and [schedule](https://flock2023.sched.com/)
* 2022: online event - [videos](https://www.youtube.com/playlist?list=PL0x39xti0_65QiQfG3g3uSvWYkRsKkxzq) and [schedule](https://events.ringcentral.com/events/fedora-mentor-summit-2022#schedule)




